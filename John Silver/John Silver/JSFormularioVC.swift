//
//  ViewController.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import UIKit

class JSFormularioVC: UIViewController {
    
    @IBOutlet weak var nombreLabel: UITextField!
    @IBOutlet weak var apellidoLabel: UITextField!
    @IBOutlet weak var telefonoLabel: UITextField!
    @IBOutlet weak var correoLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let datos: FormularioDatos = FormularioDatos(nombre: nombreLabel.text ?? "", apellido: apellidoLabel.text ?? "", telefono: telefonoLabel.text ?? "", correo: correoLabel.text ?? "")
        let destinationVC = segue.destination as! JSUserInfoVC
        destinationVC.datos = datos
    }
    
}

