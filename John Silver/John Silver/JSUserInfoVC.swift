//
//  JSUserInfoVC.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import UIKit

class JSUserInfoVC: UIViewController {
    
    var datos: FormularioDatos = FormularioDatos(nombre: "", apellido: "", telefono: "", correo: "")

    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var apellidoLabel: UILabel!
    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var correoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nombreLabel.text = self.datos.nombre
        self.apellidoLabel.text = self.datos.apellido
        self.telefonoLabel.text = self.datos.telefono
        self.correoLabel.text = self.datos.correo
    }

}
