//
//  FormularioDatos.swift
//  John Silver
//
//  Created by Gerardo Naranjo on 26/03/22.
//

import Foundation

struct FormularioDatos {
    var nombre: String
    var apellido: String
    var telefono: String
    var correo: String
}
